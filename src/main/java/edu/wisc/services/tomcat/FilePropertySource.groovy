package edu.wisc.services.tomcat

import org.apache.tomcat.util.IntrospectionUtils

class FilePropertySource implements IntrospectionUtils.PropertySource {
	static final String ENV_KEY = 'TOMCAT_PROPERTIES_FILE'
	Properties properties = new Properties()

	public FilePropertySource() {
		def filename = System.getProperty(ENV_KEY) ?: System.getenv(ENV_KEY)
		if (filename) {
			try {
				FileInputStream fis = new FileInputStream(filename)
				properties.load(fis)
				fis.close()
			} catch (e) {
				System.err.println "Could not load properties file at '$filename', caught $e"
			}
		}
	}

	@Override
	String getProperty(String key) {
		properties[key]
	}
}
