package edu.wisc.services.tomcat

import spock.lang.Specification
import static FilePropertySource.ENV_KEY

class FilePropertySourceTest extends Specification {

	def "FilePropertySource reads properties from a file specified in the environment"() {
		setup:
		File propertiesFile = File.createTempFile( "tomcat-file-property-source-test", ".properties")
		propertiesFile.deleteOnExit()
		System.setProperty(ENV_KEY, propertiesFile.getCanonicalPath())

		when:
		propertiesFile << "### Properties file header\n" +
		                  "#oldvalue=old\n" +
		                  "password=12345\n"

		then:
		'12345' == new FilePropertySource().getProperty('password')
	}

}
